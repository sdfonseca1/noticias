import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Article } from '../interfaces/interfaces';
import { ToastController } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  noticias: Article[] = [];

  constructor(private storage: Storage,
              public toastController: ToastController) {
    this.cargarFavortios();
  }
  

  guardarNoticia(noticia: Article){
    const existe = this.noticias.find(noti => noti.title === noticia.title);
    if (!existe){
      this.noticias.unshift(noticia);
      this.storage.set('Favoritos', this.noticias);
    }
    this.toastMensaje('Añadida a favoritos');
  }

  async cargarFavortios(){
    const favoritos = await this.storage.get('Favortios');
    if (favoritos){
      this.noticias = favoritos; 
    }
  }

  borrarNoticia(noticia:Article){
    this.noticias = this.noticias.filter(noti => noti.title !== noticia.title);
    this.storage.set('Favoritos', this.noticias);
    this.toastMensaje('Eliminado de favoritos');
  }

  async toastMensaje(mensaje){
    const toast = await this.toastController.create({
      message: mensaje,
      animated: true,
      color: 'primary',
      duration: 2500
    });
    toast.present();
  }

}
